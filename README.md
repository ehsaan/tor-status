# Tor Status
> A quite simple Tor indicator

It's a Python script using AppIndicator, which works on almost all desktop environments (whatever that supports GTK and AppIndicator).

Each icon color means something:
* Yellow: Tor is trying to establish a connection.
* Red: Tor service is stopped/Internet connection is missing.
* Green: Tor has successfully established a connection.

**Please note** that this script listens to SOCKS port 9050.

## Installation
1. Install the dependencies with your package manager. For instance, on Ubuntu:
```bash
$ sudo apt install gir1.2-appindicator3-0.1 python-appindicator3 python-gi
```

2. Clone the script repository:
```bash
$ git clone https://gitlab.com/Ehsaan/tor-status.git && cd tor-status
```

3. Run the script
```bash
$ python tor-status.py
```

## Contributing
Any merge requests and issues on bug tracker are welcome.

## License
WTFPL. 
